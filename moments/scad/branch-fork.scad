use <slice.scad>
use <../../parts/Branch/moments/scad/branch.scad>
use <../../parts/Fork/moments/scad/fork.scad>

$fn = 64;

diameter=40;
branch_diameter=0.8*diameter/sqrt(3);
num_branches = 3;
branch_angle = 45;
height = 180;
base_height=diameter/10;
base_thread_height=10;
stand_height = 35;

slice([1,0,0])
slice([-1.005,0,0])
union()
{

    color("white")
        rotate([45,0,120*1.5])
        translate([0,0,21.2])
        branch(height = 111, diameter = 18.5, width = 3);
    
    color("white")
        rotate([45,0,-120*0.5])
            translate([0,0,20.1])
        branch(height = 111, diameter = 18.5, width = 3);
    
    color("silver")
        rotate([0,0,0])
            fork(diameter=diameter,
                branch_angle=branch_angle,
                branch_diameter=branch_diameter,
                num_branches=num_branches);

    color("white")
        translate([0,0,-height-2])
            branch(height-base_height,diameter,width=3);

}




     
 
