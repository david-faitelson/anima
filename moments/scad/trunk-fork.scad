use <slice.scad>
use <../../parts/Branch/moments/scad/branch.scad>
use <../../parts/Fork/parts/Thread/moments/scad/fork-thread.scad>

$fn = 128;

diameter=40;
branch_diameter=0.8*diameter/sqrt(3);
num_branches = 3;
branch_angle = 45;
height = 180;
base_height=diameter/10;
base_thread_height=10;
stand_height = 35;

slice([1,0,0],size=300)
slice([-1.01,0,0],size=300)
union()
{
    fork_thread(diameter=40);
    translate([0,0,-height])
        branch(height-base_height,diameter,width=3,thread_tolerance=1);
 
}



     
 
