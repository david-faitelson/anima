use <slice.scad>
use <../../parts/Branch/moments/scad/branch.scad>
use <../../parts/Fork/moments/scad/fork.scad>
use <../../parts/Plinth/moments/scad/plinth.scad>

$fn = 128;

diameter=40;
branch_diameter=0.8*diameter/sqrt(3);
num_branches = 3;
branch_angle = 45;
height = 180;
base_height=diameter/10;
base_thread_height=10;
stand_height = 35;


for (i = [0:2])
{
    translate([0,0,260]) 
        rotate([0,-45,30+120*i]) 
            translate([0,0,50]) 
                color("white")
                   branch(height = 111, diameter = 18.5, width = 3);
}    

slice([1,0,0],size=300)
translate([0,0,-150])
union()
{

color("silver")
    translate([0,0,height+80 -25.4])
        fork(diameter=diameter,branch_angle=branch_angle,branch_diameter=branch_diameter,num_branches=num_branches);
    
    color("white")
    translate([0,0,base_height+48.5])
        branch(height-base_height,diameter,width=3);
 
color("white")
    plinth(start_height = -29, end_height = 150, step_size = 5, squash = 0.2);
}

*body(height = 111, diameter = 18.5, width = 3);

!branch(height-base_height,diameter,width=3);



     
 
