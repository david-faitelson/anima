use <../../parts/Filter/parts/Shell/moments/scad/shell.scad>
use <../../parts/Plinth/parts/Shell/moments/scad/plinth.scad>
use <tree.scad>

$fn = 128;

// slider 
skew = 1.0; //[1.0:0.02:2.0]

// slider
angle = 45; // [0:1:180]

// slider 

height = 18; // [10:100]

// slider 

width = 2; // [1:20]

// slider

level = 1; // [1:5]

echo(minangle(height,5,0.618,level));

translate([0,0,-0.5])
{
	translate([20,-10,0])
		scale(0.1)
			color("white")
				import ("../stl/ECup-Classic-60ml_V10.stl",convexity=3);
	
	translate([-28,10,1])
		scale(0.1)
			color("silver",0.99)
			rotate([-26,21.5,0])
			import("../stl/soda_can.stl",convexity=3);
	
	translate([-10,20,0])
		scale(0.16)
			color("GreenYellow")
				import("../stl/Apple.stl",convexity=3);


	color("BurlyWood")
	translate([0,0,-5])
		cube([100,100,10],center=true);
}


translate([0,5,0])
{
	translate([0,0,3.8])tree(height, width, 0.618, angle, 3, level, 1);
	color("white")
		scale(0.1)
			plinth(start_height = -29, end_height = 150, step_size = 5, squash = 0.2);
}
