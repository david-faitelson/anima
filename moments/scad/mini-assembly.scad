use <assemble.scad>
use <../../parts/Branch/moments/scad/branch.scad>
use <../../parts/Fork/moments/scad/fork-assembly.scad>
use <../../parts/Plinth/moments/scad/plinth-assembly.scad>

$fn = 32;

mini_assembly(step=true);

module mini_assembly(step=true)
{
    
    diameter=40;
    branch_diameter=0.8*diameter/sqrt(3);
    num_branches = 3;
    branch_angle = 45;
    height = 180;
    base_height=diameter/10;
    base_thread_height=10;

    path = [ 
        [ [ 0,0,0], [0,0,0] ], // plinth
        [ [ 0,0,90], [0,0,45] ], // trunk
        [ [ 0,0, 280], [0,0,222] ] // fork
    ];
    
    assemble(path=path, step=step)
    {
        plinth_assembly(step=false);
        color("white")
            branch(height-base_height,diameter,width=3);
        fork_assembly();
    }
}
