use <../../parts/Filter/parts/Shell/moments/scad/shell.scad>
use <../../parts/Plinth/parts/Shell/moments/scad/plinth.scad>
use <../../parts/Filter/parts/Bellow/parts/Bag/moments/scad/bellow.scad>
use <../../parts/Nose/parts/Shell/moments/scad/shell.scad>
use <../../parts/Nose/parts/Sensor Box/moments/scad/sensor-box.scad>

module leaf(id) 
{
		bellow_angle = map(sin($t*360+180  +(id % 5) * 45),-1,1,5,20);
		bellow_height = sin(bellow_angle)*5*25;

		translate([0,0,1.2])

		scale([0.1,0.1,0.1])
		{
			rotate([0,0,0]) 
				color("LightSteelBlue")
					hollow_receptacle();
			translate([0,0,60])
				rotate([0,180,0])
					{
						if (id == 5)
						{
							translate([0,0,30])
							rotate([0,180,30])
							{
								translate([0,0,30])
									nose_lid(height=3,radius=32);
								color("LightSteelBlue")
									nose_base(height=28, radius=32);
							}
						}
						else
						{
							color("Tomato",0.99)
								bellow(opening_angle=bellow_angle);
						color(id == 18 ? "CadetBlue" : 
								id == 3 ? "CadetBlue" :
								id == 5 ? "CadetBlue" : 
								id == 15 ? "SeaGreen" :
								id == 12 ? "SeaGreen" :
								id == 7 ? "SeaGreen" :
								"DimGray")
							translate([0,0,bellow_height])
								rotate([0,0,30])
                                    cylinder(h=5,r=48,$fn=6);
						}
					}
		}
}


function minangle(b,r,s,n) =
	n == 0 ? 2*asin(r/b) : minangle(b*s,r,s,n-1);

function branch_r(r, n, b=3) =

	n == 0 ? r : branch_r(0.8*r/sqrt(b),n-1,b);

module tree(h, r, s, alpha, b, n, id)
{
	color("white",0.99)
		cylinder(h=h,r=r);

	if (n == 0) 
	{
		translate([0,0,h])
			translate([0,0,-1]) 
				leaf(id);
	}

	if (n > 0) {
	
		for(i = [0:b-1]) {
			translate([0,0,h])
				rotate([alpha,0,(i-0.5)*360/b])
						tree(h*s*pow(1 /*skew*/,i),0.8*r/sqrt(b),s,alpha,b,n-1,id+i*pow(b,n-1));
			}

		translate([0,0,h+0])
				color("silver",0.99)
					sphere(r);
	}
}

