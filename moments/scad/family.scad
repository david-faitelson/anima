
use <../../parts/Plinth/parts/Shell/moments/scad/plinth.scad>

use <tree.scad>

$fn = 128;

// slider 
skew = 1.0; //[1.0:0.02:2.0]

// slider
angle = 45; // [0:1:180]

// slider 

height = 18; // [10:100]

// slider 

width = 2; // [1:20]

// slider

level = 1; // [1:5]

echo(minangle(height,5,0.618,level));

color("LightGrey",0.2)
translate([50,80,83])
	import("../stl/malestanding2.stl", convexity=3);

union()
{
translate([30,0,0])
	scale([0.1,0.1,0.1])
	{
		rotate([0,0,41])
		{
			color("black")
				translate([0,0,5])
				cylinder(h=5,r=48,$fn=6);
			color("white")
				cylinder(h=5,r=48,$fn=6);
		}
	}

translate([20,10,0])
	scale([0.1,0.1,0.1])
	{
		rotate([0,0,15])
		{
			color("black")
				translate([0,0,5])
				cylinder(h=5,r=48,$fn=6);
			color("white")
				cylinder(h=5,r=48,$fn=6);
		}
	}

translate([-20,5,0])
	scale([0.1,0.1,0.1])
	{
		rotate([0,0,27])
		{
			color("black")
				translate([0,0,5])
				cylinder(h=5,r=48,$fn=6);
			color("white")
				cylinder(h=5,r=48,$fn=6);
		}
	}
}

translate([0,5,0])
{
	tree(height, width, 0.618, angle, 3, level, 1);
	color("white")
		scale(0.1)
			plinth(start_height = -29, end_height = 150, step_size = 5, squash = 0.2);
}

translate([60,0,0]) 
{
	translate([0,0,5.0])
		rotate([0,0,-120])
		tree(28, 3, 0.618, 45, 3, 2, 1);
	color("white")
		scale(0.135)
			plinth(start_height = -29, end_height = 150, step_size = 5, squash = 0.2);
}

translate([150,-3,0]) 
{
	translate([0,0,7])
	tree(48, 5, 0.618, 45, 3, 3, 1);
	color("white")
		scale(0.21)
			plinth(start_height = -29, end_height = 150, step_size = 5, squash = 0.2);
}
