use <slice.scad>
use <../../parts/Branch/moments/scad/branch.scad>
use <../../parts/Filter/parts/Shell/moments/scad/case.scad>

$fn = 64;

diameter=40;
branch_diameter=0.8*diameter/sqrt(3);
num_branches = 3;
branch_angle = 45;
height = 180;
base_height=diameter/10;
base_thread_height=10;
stand_height = 35;

slice([-1,0,0],size=200)
union()
{
translate([0,0,-101])
    color("white")
    branch(height = 111, diameter = 18.5, width = 3);

translate([0,0,20])case();
}



     
 
