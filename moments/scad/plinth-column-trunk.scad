use <slice.scad>
use <../../parts/Branch/moments/scad/branch.scad>
use <../../parts/Plinth/parts/Shell/parts/Column/moments/scad/plinth-column.scad>

$fn = 128;

diameter=40;
branch_diameter=0.8*diameter/sqrt(3);
num_branches = 3;
branch_angle = 45;
height = 180;
base_height=diameter/10;
base_thread_height=10;
stand_height = 35;

slice([1,0,0],size=300)
slice([-1.01,0,0],size=300)
translate([0,0,-120])
union()
{
    color("white")
    translate([0,0,base_height+42.3])
        branch(height-base_height,diameter,width=3,thread_tolerance=2);
 
color("white")
    plinth_column(base_diameter = 2 * (2/15) * 150 * sqrt(3));
}



     
 
