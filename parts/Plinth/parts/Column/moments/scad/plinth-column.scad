use <threads.scad>
use <slice.scad>
use <shapes.scad>
use <fillet.scad>
use <standoffs.scad>
use <Metric/files/M3.scad>

$fn = 128;

//*slice([1,0,0],size=400)
 
plinth_column(base_diameter = 2 * (2/15) * 150 * sqrt(3));

module plinth_column(height=16,end_height = 150, base_diameter, top_diameter=40)
{
    
    difference()
    {
        
        threaded_column(
            height=16,
            base_diameter=base_diameter,
            top_diameter=top_diameter,
            thread_length=10,
            thread_diameter=40*0.9);
    
        // cut a hole in the column for the power socket
            
        rotate([0,0,25])
            translate([0,-base_diameter/2-1, 7])
        {
            intersection()
            {
                translate([0,4,0])
                    rotate([90,0,0])cylinder(h=4,d=8+0.2);
                translate([-(8+0.2)/2,0,-(6.5+0.2)/2])
                    cube([8+0.2,8+0.2,6.5+0.2]);
            }
        }
    }
    
    // add a ring to hold the standoffs for the pcb
    translate([0,0,25-0.1])
    {
        difference()
        {
            ring(height=4, outer_diameter=base_diameter-5, outer_diameter_bottom=base_diameter-5, inner_diameter=base_diameter-22, nfaces=$fn);
            translate([-base_diameter/2,-15,-1])cube([base_diameter,30,6]);
            rotate([0,0,90])translate([-base_diameter/2,-15,-1])cube([base_diameter,30,6]);
            
        // cut holes for the M3 standoffs

        translate([-38.61/2,-38.61/2,-5])
        for(i=[0:1])
            for(j=[0:1])
            {
                translate([38.61*i,38.61*j,0])
                    scale([1,1,-1])
                        BoltM3();
            
            }
        }
    }    
}

module threaded_column(height,base_diameter, top_diameter, thread_diameter, thread_length)
{
    difference()
    {
        column(height=height,base_diameter=base_diameter,top_diameter=top_diameter);

        translate([0,0,column_length(height=height,base_diameter=base_diameter,top_diameter=top_diameter)])
        {
            translate([0,0,-thread_length])
                cylinder(h=thread_length,d=top_diameter);
        }
    }

    translate([0,0,column_length(height=height,base_diameter=base_diameter,top_diameter=top_diameter)])
        {
            scale([1,1,-1])
            difference()
            {
                cylinder(h=thread_length,d=top_diameter);
                translate([0,0,-0.1])metric_thread(length=thread_length+1, diameter = thread_diameter,pitch = 2);
            }
        }
    
}

function column_length(height,base_diameter,top_diameter) = height+sqrt(base_diameter*base_diameter/4-top_diameter*top_diameter/4);

module column(height,base_diameter, top_diameter)
{
    tube(height=height,diameter=base_diameter,width=2);
    translate([0,0,height-0.01])
        truncated_hollow_dome(base_diameter=base_diameter,top_diameter=top_diameter,width=2);
}
