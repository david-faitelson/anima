use <assemble.scad>
use <plinth-column.scad>
use <NopSCADlib/vitamins/pcb.scad>

use <../../parts/Power Socket/moments/scad/power-socket.scad>
use <../../parts/Circuit/moments/scad/controller-circuit.scad>

plinth_column_assembly();

module plinth_column_assembly(step=true)
{
    diameter = 2 * (2/15) * 150 * sqrt(3);
        
    color("white")
        plinth_column(base_diameter = diameter);

    path = [ 
        [ [-38.61/2,-38.61/2,-50], [-38.61/2,-38.61/2,15] ], // standoffs
        [ [0,0,-100], [0,0,14] ], // circuit
        [ [20,-40,0], [0,0,0] ], // power socket
    ];
    
    assemble(path=path, step=step)
    {
        
        union()
        {
            for(i=[0:1])
                for(j=[0:1])
            {
                translate([38.61*i,38.61*j,0])
                    standoff(h=10,d=5,h2=18,d2=3);
            }
        }
        
        controller_circuit();
    
        rotate([0,0,25])
            translate([0,-diameter/2-4,8])
                rotate([-90,0,0])
                    power_socket();
    }

}