use <threads.scad>
use <slice.scad>
use <shapes.scad>
use <fillet.scad>
use <standoffs.scad>
use <plinth-column.scad>

$fn = 128;

slice([0,0,1.6])
slice([0,1,0])
 
plinth_column(base_diameter = 2 * (2/15) * 150 * sqrt(3));
