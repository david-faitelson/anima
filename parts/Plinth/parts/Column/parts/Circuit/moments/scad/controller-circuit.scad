use <Arduino-Nano.scad>
use <../../parts/Voltage regulator/moments/scad/voltage-regulator.scad>

controller_circuit();

module controller_circuit()
{
    color("green")
        translate([-108.7,106.7,0]) import( "../stl/controller-pcb.stl", convexity=3);
    translate([-9,-20,1])
    *color("blue") 
        difference()
        {
            cube([17,40,17]);
            translate([2,-0.5,-0.1])cube([13,42,11]);
        }
    translate([-23,0,10])
        rotate([0,90,90])
        voltage_regulator();
        
    translate([0,0,14])
        rotate([0,0,90])scale([1,1,-1])nano_328_v1();
    
}

