use <slice.scad>
use <shapes.scad>

$fn = 32;

*!
union()
{
    weight_clasp(radius=10.2);
    translate([35,0,0])
    weight_clasp(radius=10.4);
}

//slice([-0.9,0,0],size=400)
//rotate([0,0,60])
plinth_dress(start_height = -29, end_height = 150, step_size = 5, squash = 0.2);

module rib(offset=20)
{
    translate([0,-2,0])
        intersection()
        {
            translate([offset,0,0])
                {
                    difference()
                    {
                        cube([150-offset-4,4,(150- -29)*0.2]);
                        for(i=[0:3])
                            translate([20+ 25*i,4.5,17.5 - i*4/2])
                                rotate([90,0,0])
                                    cylinder(h=5,d=20-i*4);
                    }
                }
            rotate([90,0,0])
                scale([1,0.2,1])
                    translate([0,28,-4])cylinder(h=4,r=147);
        }
}
    
module weight_clasp(radius=10.2)
{
    rotate([0,0,-17.5])
    rotate_extrude(angle=215)
        translate([radius,0]) square([4,4]);
}

module plinth_dress(start_height = -29, end_height = 150, step_size = 5, squash = 0.2, top_diameter=40)
{
    sphere_radius = (2/15)*end_height*sqrt(3);
    
    base_diameter = 2 * sphere_radius;
    
    difference()
    {
        union()
        {
            translate([0,0,-start_height*squash])
                base_shell(start_height = -29, end_height = 150, step_size = 5, squash = squash);
        
            // add the ribs
        for(i=[0:5])
            rotate([0,0,30+60*i])
                translate([0,-2,0])
                    rib(offset=29);
        }

        translate([0,0,-0.2])
            column_block(height=16.2,base_diameter=base_diameter+0.5,top_diameter=top_diameter+0.5);
        
        // cut a hole in the shell for the power cable
        
        translate([10,-end_height+15, 1]) rotate([90,0,30]) cylinder(h=12,r=5);
    
    }
    
    // add the weight clasps

    for(i=[0:5])
        rotate([90,0,-60*i])
        {
            translate([90,12,0])
            {
                for (i=[-1:2:1])
                    translate([0,0,30*i - 2])
                        weight_clasp();
            }
        }
        
    
}

module column_block(height,base_diameter, top_diameter)
{
    cylinder(h=height,d=base_diameter);
    translate([0,0,height-0.01])
        truncated_dome(base_diameter=base_diameter,top_diameter=top_diameter);
}

module base_shell(start_height = -30, end_height=60, step_size = 5, squash = 0.2)
{
    sphere_radius = (2/15)*end_height*sqrt(3);
    
    difference()
    {
        hexagonal_shape(squash,start_height,end_height,step_size);
        translate([0,0,-2])
            hexagonal_shape(squash,start_height,end_height-5,step_size);
    }
}

module hexagonal_shape(squash,start_height,end_height,step_size)
{
    rotate([0,0,30])
        scale([1,1,squash])
            hull()
                for(h = [start_height:step_size:end_height])
                {
                    translate([0,0,h])
                        linear_extrude(height=1)
                            hexagon(radius=sqrt(end_height*end_height-h*h));
                }
}
