use <slice.scad>

$fn = 128;

for(i=[0:2:4])
{
    translate([20*i,0,0])
        break(nparts=6, separation=0)
        {
            cylinder_jigsaw_slice(height=2, diameter=45,angle=60,notch_width=3, tolerance=0.1+0.1*i);
            translate([-15,-15,0]) 
                cube([30,30,2]);
        }
    echo(0.1+0.1*i);
}

