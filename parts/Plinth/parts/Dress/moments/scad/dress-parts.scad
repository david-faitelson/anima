use <slice.scad>
use <shapes.scad>
use <math.scad>
use <dress.scad>

$fn = 128;

x = 20;//map(sin($t*360 - 90),-1,1,0,20);
 
break(nparts=6, separation=x)
{
    cylinder_jigsaw_slice(height=60, diameter=304,angle=60,notch_width=3,notch_depth=2, tolerance=0.3);

    plinth_dress(start_height = -29, end_height = 150, step_size = 5, squash = 0.2);
}
