use <slice.scad>
use <assemble.scad>
use <dress.scad>
use <../../parts/Weight/moments/scad/weight.scad>

$fn = 32;

plinth_dress_assembly(step=true);

module plinth_dress_assembly(step=true)
{
    path = [ 
        [ [ 0,0,0], [0,0,0] ], // dress
        [ [ 0,0,-100], [0,0,0] ] // weights
    ];
    
    assemble(path=path, step=step)
    {
        color("white")
            plinth_dress(start_height = -29, end_height = 150, step_size = 5, squash = 0.2);
        
        for(i=[0:5])
        rotate([90,0,-60*i])
        {
            translate([90,12,0])
            {
                weight();
            }
        }

    }
}