use <plinth.scad>

$fn = 128;

//slice([1,0,0],size=400)
union()
{
 
    plinth(start_height = -29, end_height = 100, step_size = 5, squash = 0.2);
}
