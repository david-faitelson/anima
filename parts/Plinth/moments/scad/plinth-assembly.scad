use <assemble.scad>

use <../../parts/Column/moments/scad/plinth-column-assembly.scad>
use <../../parts/Dress/moments/scad/dress-assembly.scad>

$fn = 32;

plinth_assembly();

module plinth_assembly(step=true)
{  
    start_height = -29;
    end_height = 150;
    step_size = 5;
    squash = 0.2;
    top_diameter = 40;

    plinth_dress_assembly(step=false);

    path = [ 
        [ [0,0,-100], [0,0,0] ] // column
    ];

    assemble(path=path,step=step)
    {   
        plinth_column_assembly(step=false);
    }
}
