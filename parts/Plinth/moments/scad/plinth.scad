use <threads.scad>
use <slice.scad>
use <shapes.scad>
use <fillet.scad>
use <standoffs.scad>
use <../../parts/Column/moments/scad/plinth-column.scad>
use <../../parts/Dress/moments/scad/dress.scad>

$fn = 32;

slice([1,0,0],size=400)
 
plinth(start_height = -29, end_height = 150, step_size = 5, squash = 0.2);

module plinth(start_height = -29, end_height = 150, step_size = 5, squash = 0.2, top_diameter=40)
{

    plinth_column(base_diameter = 2 * (2/15) * end_height * sqrt(3));
    
    plinth_dress(
        start_height = start_height, 
        end_height = end_height, 
        step_size = step_size, 
        squash = squash);
}

