use <slice.scad>
use <shapes.scad>
use <Metric/files/M2.scad>
use <standoffs.scad>
use <polygonal-box.scad>

$fn = 128;

//slice([0,-1.0,0])
sensor_box(radius=34,height=38);

module sensor_box(height=28,radius=32)
{
    nose_base(height=height, radius=radius);
}

module nose_base(height=30, radius=32)
{
    difference()
    {
        polygonal_box_with_screw_holes(height=height,radius=radius,nfaces=6);
        translate([0,0,-1])
            cylinder(h=4, r=6);
        
        translate([0,0,-0.1])
        for(i=[0:2])
            rotate([0,0,i*360/3])
                translate([10,0,0])
                    cylinder(h=3+1,d=3.3);

    }

    translate([-17.78/2,-26.92/2])
    for(i=[0:1])
        for(j=[0:1])
        {
            translate([17.78*i,26.92*j,0])
                standoff(shape=1, baseHeight=12, baseDiameter=6, style=2, topHeight=3, topDiameter=3);
        }

}
