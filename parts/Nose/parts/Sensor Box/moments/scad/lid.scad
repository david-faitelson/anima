use <slice.scad>
use <shapes.scad>
use <Metric/files/M2.scad>
use <polygonal-box.scad>

$fn = 128;

//slice([0,-1.0,0])
    
nose_lid(height=3,radius=34);

module nose_lid(height=3, radius=32)
{
	color("PaleVioletRed",0.99)
	difference()
	{
		polygonal_box_with_screw_holes(height=height,radius=radius,nfaces=6);
        
		translate([0,5.5,height-5])
			scale([1.618,1,1])
				cylinder(h=6,r=5,$fn=6);
		translate([0,-5.5,height-5])
			scale([1.618,1,1])
				cylinder(h=6,r=5,$fn=6);
	}

	*color("black",0.4)
	translate([0,10,height-4])
		scale([1.618,1,1])
			cylinder(h=1,r=5,$fn=6);
	*color("black",0.4)
	translate([0,-10,height-4])
		scale([1.618,1,1])
			cylinder(h=1,r=5,$fn=6);


    translate([-21,-15,-6])
    {
        difference()
        {
            cube([6,30,6]);
            translate([4-0.2,-0.1,2])
                cube([2.4,30.2,2.4]);
        }
    }

    translate([15,-15,-6])
    {
        difference()
        {
            cube([6,30,6]);
            translate([-0.2,-0.15,2])
                cube([2.4,30.2,2.4]);
        }
    }
}