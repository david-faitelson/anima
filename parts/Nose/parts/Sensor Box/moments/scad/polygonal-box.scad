use <slice.scad>
use <shapes.scad>
use <Metric/files/M2.scad>
use <standoffs.scad>

$fn = 64;

polygonal_box_with_screw_holes(height=28,radius=32,nfaces=6);

module polygonal_box_with_screw_holes(height, radius, nfaces)
{
    difference()
    {
        polygonal_box_with_columns(height=height, radius=radius, nfaces=nfaces);
        for(i=[0:nfaces-1])
        {
            rotate([0,0,i*360/nfaces])
                translate([radius-2.5,0,0])
                {
                        translate([0,0,height+2.1])
                            BoltM2(l=6);
                }
        }
    }
}

module polygonal_box_with_columns(height, radius, nfaces)
{

    polygonal_box(height=height,radius=radius,nfaces=nfaces);
    for(i=[0:nfaces-1])
    {
        rotate([0,0,i*360/nfaces])
            translate([radius-2.5,0,0])
            {
                    cylinder(h=height,d=5);
            }
    }
}

module polygonal_box(height, radius, nfaces)
{
    difference()
    {
        cylinder(h=height,r=radius,$fn=nfaces);
        translate([0,0,2])
            cylinder(h=height+2,r=radius-2,$fn=6);
    }
}