use <slice.scad>
use <shapes.scad>
use <fillet.scad>
use <shell.scad>

$fn = 128;

//slice([0,-1,0],size=400)
nose_shell(stalk_height=28);

module nose_shell(stalk_height)
{
    stalk(height=stalk_height, radius=12);
    shell();
}

module stalk(height,radius)
{
 
    tube(height=height,diameter=2*radius,width= radius - 8);
    translate([0,0,height])
    {
        for(i=[0:2])
            rotate([0,0,i*360/3])
                translate([radius-2,0,0])
                    cylinder(h=3,d=3);
    }
    
}

