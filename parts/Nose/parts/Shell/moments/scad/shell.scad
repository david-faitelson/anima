use <shapes.scad>
use <slice.scad>

$fn = 32;

//slice([1,0,0],size=400)
shell(step_size=1);

translate([120,0,2.5])shell(step_size=5);

module shell(start_height=-30,end_height=65,step_size=1)
{
    hollow_receptacle(start_height = -30, end_height=end_height, step_size = step_size);
	
    // y = ax + b
    // 0 = a*5 + b
    // 2.5 = a*1 + b
    // 2.5 = a -5a
    // 2.5 = -4a
    // a = 2.5/-4
    // b = 5*2.5/4
    
    translate([0,0,0.01 + -(2.5/4)*step_size + 5*2.5/4])
        rotate([0,0,30])
            hollow_hexagonal_base(start_height=17.5,end_height=25.0,radius=25.0);
}

module hexagonal_base(start_height=16.5,end_height=25,radius=25)
{
    translate([0,0,-end_height])
    for(h = [start_height:1.0:end_height])
        {
            hull()
            {
                translate([0,0,h])
                    linear_extrude(height=0.1)
                        circle(r=radius - sqrt(radius*radius-h*h),$fn=6);
                translate([0,0,h+2.5])
                    linear_extrude(height=0.1)
                        circle(r=radius - sqrt(radius*radius-(h+2.5)*(h+2.5)),$fn=6);
            }
        }
}

module hollow_hexagonal_base(start_height=16.5,end_height=25,radius=25, hole_radius=4.0)
{
    thread_tolerance = 1;
    
	difference()
	{
		hexagonal_base(start_height=start_height,end_height=end_height,radius=radius);
		*scale(0.5) hexagonal_base(start_height=start_height,end_height=end_height,radius=radius);
       translate([0,0,-(end_height-start_height)/2]) cylinder(h=1.1*(end_height-start_height),r=hole_radius,center=true);
	}
    
    translate([0,0,-(end_height-start_height)+0.01])
        bottom_thread(depth=10, diameter=0.9*18.5 - thread_tolerance, width=4.325-thread_tolerance/2);
}


module receptacle(start_height = -30, end_height=60, step_size = 5)
{
    scale([1,1,-1])
        translate([0,0,-(end_height-step_size+1)*0.618])
        rotate([0,0,30])
            scale([1,1,0.618])
                hull()
                    for(h = [start_height:step_size:end_height])
                    {
                        translate([0,0,h])
                            linear_extrude(height=1)
                                hexagon(radius=sqrt(end_height*end_height-h*h));
                    }

}

module hexagon(radius) 
{
	circle(r=radius,$fn=6);
}

module hollow_receptacle(start_height = -30, end_height=62, step_size = 5)
{
	difference()
	{
		receptacle(start_height,end_height,step_size);
		translate([0,0,1.5])
            receptacle(start_height-2,end_height-2,step_size);
	translate([0,0,-step_size/5])
		cylinder(h=step_size+1,r=4);
	}
}
module bottom_thread(depth, diameter, width)
{
	translate([0,0,-depth])
		hollow_thread(height=depth, diameter = diameter, width = width, 
pitch = 2);
}



