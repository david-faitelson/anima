$fn = 128;

dust_sensor();

module dust_sensor()
{
	color("silver",1)
	difference()
	{
		union()
		{
			hull()
			{
				translate([20,14,1]) 
					cylinder(h=2,r=3,center=true);
				translate([-20,14,1]) 
					cylinder(h=2,r=3,center=true);
				translate([20,-14,1]) 
					cylinder(h=2,r=3,center=true);
				translate([-20,-14,1]) 
					cylinder(h=2,r=3,center=true);
			}		
			
			translate([0,0,17.6/2])
				cube([46,30,17.6],center=true);

		}
		translate([3,-18.7,-0.5])
			cube([20,1+17-14.3,3]);
		translate([0,0,9])
			cylinder(h=20,d=8,center=true);

		translate([-24,17.5,12]) 
			rotate([45,0,0])
				cube([48,10,10]);

		translate([-24,-17.5,12]) 
			rotate([45,0,0])
				cube([48,10,10]);

		translate([-20,-15.3,17.6-3+0.1])
			cube([13.4, 9.3, 3]);



	}

	color("blue")
		translate([-20,-6.3-0.1,17.6-3+0.1])
			cube([13.4, 5, 3]);

//	translate([22.2,15-3,17.6-3]) cube([1,3,3]);
}
