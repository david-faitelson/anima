use <slice.scad>
use <shapes.scad>
use <fillet.scad>
use <../../parts/Sensor Box/moments/scad/sensor-box.scad>
use <../../parts/Shell/moments/scad/nose-shell.scad>

$fn = 64;

slice([0,-1,0],size=400)
union()
{

    translate([0,0,4/*28*/])
    {
        rotate([0,0,30])nose_base(height=28,radius=34);
    }
    
    nose_shell(stalk_height=0/*28*/);
}


