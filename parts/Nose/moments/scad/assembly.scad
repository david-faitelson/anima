use <slice.scad>
use <shapes.scad>
use <fillet.scad>
use <../../parts/Sensor Box/moments/scad/sensor-box.scad>
use <../../parts/Sensor Box/moments/scad/lid.scad>
use <../../parts/Dust Sensor/moments/scad/dust-sensor.scad>
use <../../parts/Shell/moments/scad/nose-shell.scad>

$fn = 50;

slice([0,1,0],size=400)
union()
{

    translate([0,0,19])
    {
            sensor_box(height=38,radius=34);

            *translate([0,0,40])
                nose_lid(height=3,radius=34);

            *translate([0,0,38.2])
                rotate([0,180,90])
                    dust_sensor();

    }
    
    nose_shell(stalk_height=19);
}


