use <slice.scad>
use <math.scad>
use <steppers/stepper.scad>
use <standoffs.scad>
use <Metric/files/M3.scad>
use <../../parts/Shell/moments/scad/case.scad>
use <../../parts/Motor Damper/moments/scad/damper.scad>
use <../../parts/Piston/moments/scad/piston.scad>
use <../../parts/Bellow/moments/scad/bellow.scad>
use <../../parts/Lid/moments/scad/lid.scad>
use <../../parts/Circuit/moments/scad/filter-circuit.scad>

/*

Nema 17 42x42x20 cm, weight 140g

*/

$fn=64;

arm_length = 33;

handle_length = 24;

handle_angle = 0;//map(sin($t*360),-1,1,-180,180);

arm_angle = asin(((handle_length-3)/arm_length)*sin(handle_angle));

bellow_height = 0 + arm_length*cos(arm_angle) +  (handle_length-3) * cos(handle_angle);

// ruler for bellow amplitude

translate([0,65,76-25]) cube([4,4,40+25]);

slice([-1.0,0.0,0],size=130)
    case();

// pcb electronics bounds
*#translate([-26,0,22.5+3])
    cube([30,60,40],center=true);

translate([35 ,-8,31]) 
rotate([0,0,0])
	rotate([0,-90,0]) 
    {
        translate([0,8,23.0]) 
            rotate([0,0,45])
                damper();
        
        translate([0,8,0]) 
            NEMA17(length=23,shaft_length=22);
    }

// circuit circuit & standoffs
    
*translate([-45.5 +11,1.0,31])
{
    rotate([180,-90,0])
        filter_circuit();
    
    for(i=[0:11:11])
        for(j=[-1:2:1])
        {
            translate([0,-4*6+1+4*i,4*3*j])
            {
                rotate([0,-90,0])
                {
                    color("silver")
                    standoff(shape=2, baseHeight=4, baseDiameter=6, style=1, topHeight=4, topDiameter=3);
                    *translate([0,0,17])NutM3();
                }
            }
        }
}
    
translate([-10,0,0])
    color ("red") cube([5.5,0.25,0.25]);

// arm and handle
    
translate([-1.5,0,31])
	rotate([0,-90,0])
        crank(handle_angle=handle_angle, handle_length=handle_length, arm_length=arm_length);

// bellow
    
*translate([0,0,52+bellow_height+6])
    bellow(radius=50,height=bellow_height+6);

*translate([0,0,61.5+bellow_height])
    lid(diameter=114);

