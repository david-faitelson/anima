use <slice.scad>
use <math.scad>
use <steppers/stepper.scad>
use <standoffs.scad>
use <Metric/files/M3.scad>
use <../../parts/Shell/moments/scad/case.scad>
use <../../parts/Motor Damper/moments/scad/damper.scad>

/*

Nema 17 42x42x20 cm, weight 140g

*/

$fn=64;

slice([-1.0,0.0,0],size=130)
    case();

translate([35.6,-8,31]) 
	rotate([0,-90,0]) 
    {
        translate([0,8,23.0]) rotate([0,0,45])damper();
        
        translate([0,8,0]) 
        difference()
        {
            NEMA17(23);
            translate([0,0,37]) cylinder(h=11,r=5);
        }
    }

