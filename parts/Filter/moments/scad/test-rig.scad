use <slice.scad>
use <math.scad>
use <steppers/stepper.scad>
use <standoffs.scad>
use <Metric/files/M3.scad>
use <../../parts/Piston/moments/scad/piston.scad>
use <../../parts/Bellow/moments/scad/bellow.scad>
use <../../parts/Lid/moments/scad/lid.scad>
use <../../parts/Shell/parts/BellowBase/moments/scad/bellow-base.scad>

/*

Nema 17 42x42x20 cm, weight 140g

*/

$fn=128;

arm_length = 32;

handle_length = 24;

handle_angle = map(sin($t*360),-1,1,-180,180);

arm_angle = asin(((handle_length-3)/arm_length)*sin(handle_angle));

bellow_height = 0 + arm_length*cos(arm_angle) +  (handle_length-3) * cos(handle_angle);

// ruler for bellow amplitude

*translate([0,30,78]) cube([4,4,40]);

translate([45 ,-8,31]) 
rotate([0,0,0])
	rotate([0,-90,0]) 
    {
        
        translate([0,8,0]) 
        difference()
        {
            NEMA17(23);
        }
    }  
    
// arm and handle
    
translate([0,0,31])
	rotate([0,-90,0])
        crank(handle_angle=handle_angle, handle_length=handle_length, arm_length=arm_length);

// bellow base + legs (test rig)
!union()
{
    translate([0,0,56])bellow_base();
        
    // bellow legs
    for(i=[0:5])
    {
        rotate([0,0,30+60*i])
            translate([60,0,0])
                cylinder(h=56,r=3);
    }
}

// bellow
    
translate([0,0,52+bellow_height+15])
    bellow(radius=50,height=bellow_height+6);

translate([0,0,62.5+bellow_height])
    lid(diameter=84);

