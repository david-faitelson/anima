use <slice.scad>
use <../../parts/Lid/moments/scad/lid.scad>
use <../../parts/Bellow/parts/Roof/moments/scad/bellow-roof.scad>
use <../../parts/Bellow/parts/Bag/moments/scad/bellow-bag.scad>
use <../../parts/Bellow/parts/Bed/moments/scad/bellow-bed.scad>

$fn=128;

bellow_radius = 47;

bellow_height = 40;

bellow_angle = asin((bellow_height-11-7)/(6*25));

translate([0,0,12]) lid(diameter=110);

translate([0,0,-bellow_height-4])
    rotate([0,0,30])
        bellow_bed();

translate([0,0,5])
    bellow_roof();

bellow_bag(radius=bellow_radius,opening_angle=bellow_angle,n=7);




