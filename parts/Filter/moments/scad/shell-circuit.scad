use <slice.scad>
use <math.scad>
use <steppers/stepper.scad>
use <standoffs.scad>
use <Metric/files/M3.scad>
use <../../parts/Shell/moments/scad/case.scad>
use <../../parts/Circuit/moments/scad/filter-circuit.scad>


$fn=64;

slice([1.0,0.0,0],size=130)
    case();

// pcb electronics bounds
*#translate([-26,0,22.5+3])
    cube([30,60,40],center=true);


// circuit circuit & standoffs
    
translate([-35 +10,1.0,25.6])
{
    rotate([180,-90,0])
        filter_circuit();
    
    for(i=[0:11:11])
        for(j=[-1:2:1])
        {
            translate([0,-4*6+1+4*i,4*3*j])
            {
                rotate([0,-90,0])
                {
                    color("silver")
                    standoff(shape=2, baseHeight=10, baseDiameter=6, style=1, topHeight=4, topDiameter=3);
                    *translate([0,0,17])NutM3();
                }
            }
        }
}
    
