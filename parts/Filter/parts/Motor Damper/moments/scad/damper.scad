use <shapes.scad>

$fn = 128;

damper();

module damper()
{
    color("silver") plank();
    color("red")
        translate([0,0,1.6+0.01])
            tube(height=2.7-0.1,diameter=30,width=7);
    translate([0,0,4.3])
        rotate([0,0,90])
            color("silver") plank();
}

module plank(height=1.6)
{
    difference()
    {
        hull()
        {
            translate([-43.84/2,0,0])
                cylinder(h=1.6,d=8);
            
            cylinder(h=height,d=32);
            
            translate([43.84/2,0,0])
                cylinder(h=height,d=8);
        }
        
            translate([-43.84/2,0,-0.25])
                cylinder(h=height+1,d=3.5);
            
            translate([0,0,-0.25])
                cylinder(h=height+1,d=23);
        
            translate([43.84/2,0,-0.25])
                cylinder(h=height+1,d=3.5);
        
    }
}
