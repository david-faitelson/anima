use <shapes.scad>
use <pin.scad>
use <slice.scad>
use <fillet.scad>
use <Metric/files/M2.scad>
use <../../parts/Handle/moments/scad/piston-handle.scad>
use <../../parts/Arm/moments/scad/piston-arm.scad>
use <../../parts/Cap/moments/scad/shaft-cap.scad>

$fn=128;

slice([0,1,0],size=150)
slice([0,-1.01,0],size=150)
crank(handle_angle=0, handle_length=18, arm_length=33);

*translate([-20,0,-2.5])
    rotate([0,-90,0])
        BoltM2(l=6);

*translate([-5.5,0,-2.5+12])
    rotate([0,-90,0])
        NutM2();

module crank(handle_angle, handle_length, arm_length)
{
    arm_angle = asin(((handle_length-3)/arm_length)*sin(handle_angle));

    rotate([0,0,handle_angle])
    {
        color("purple")
        {
            translate([0,0,-7])
                handle(length = handle_length);
        }

        translate([handle_length-2.5,0,3.6])
        {

            translate([0,0,3.2])shaft_cap();


            rotate([0,0,-handle_angle - arm_angle])
            {
                arm(length=arm_length,width= 8);
                translate([arm_length,0,3.2])
                    shaft_cap();
            }
        }
    }
}