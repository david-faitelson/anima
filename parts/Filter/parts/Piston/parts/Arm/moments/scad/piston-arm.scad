use <shapes.scad>
use <pin.scad>
use <slice.scad>
use <fillet.scad>
use <Metric/files/M2.scad>

$fn=128;

*bent_arm(length = 33, width=8);

arm(length = 33, width=8);

// modules

module arm(length, width)
{
	color("SeaGreen",0.8)
        difference()
        {
            hull()
            {
                cylinder(h=3,d=width,center=true);
                translate([length,0,0])
                {
                    cylinder(h=3,d=width,center=true);
                }
            }
            translate([0,0,-2]) 
                cylinder(h=4,d=4.5);
            translate([length,0,-2.1]) 
                cylinder(h=4.2,d=4.5);
        }

}

module bent_arm(length, width)
{
    height = 11+3;
    
	color("SeaGreen",0.8)
        difference()
        {
            hull()
            {
                cylinder(h=height,d=width);
                translate([length,0,0])
                {
                    cylinder(h=height,d=width);
                }
            }
            translate([-1,0,height/2 + 3])
                cube([16,width+1,height+0.3],center=true);
            translate([length+1,0,height/2 + 3])
                cube([11,width+1,height+0.3],center=true);
            translate([length/2+1.25,0,height/2-3.5])
                cube([length/2-3,width+1,height],center=true);
            
            translate([0,0,-0.1]) 
                cylinder(h=4,d=4.5+0.2);
            translate([length,0,-0.1]) 
                cylinder(h=4.2,d=4.5+0.2);
        }
        
    translate([length/2+length/4-0.25,width/2,height-3-0.5])rotate([0,180,90])
        linear_fillet(length=width,radius=2);

    translate([length/2-length/4+3-0.25,-width/2,height-3-0.5])rotate([180,0,90])
        linear_fillet(length=width,radius=2);
}
