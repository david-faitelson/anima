use <threads.scad>

$fn=32;

shaft_cap();

module shaft_cap()
{
    difference()
    {
        cylinder(h=4,d=8);
        translate([0,0,-0.1])
        metric_thread (diameter=3+0.4, pitch=0.5, length=2.2);
    }
}


