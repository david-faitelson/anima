use <shapes.scad>
use <pin.scad>
use <slice.scad>
use <fillet.scad>
use <threads.scad>
use <Metric/files/M2.scad>

$fn=128;

handle(length = 22);

*translate([0,20,0]) handle(length=23);

*translate([0,40,0]) handle(length=24);

module handle(length)
{
	difference()
	{
        union()
        {
            cylinder(h=5,d=17.5);
            translate([0,0,5-0.01])
                handle_face(diameter=17.5,length=length,width=5);
        }

        difference()
        {
            translate([0,0,-0.1])
                cylinder(h=5+5+1,d=5 + 0.5);

            translate([-2.5-0.4,-2.5,0])
                cube([0.6,5,10]);
        }
        
        hull()
        {
            translate([-5.5,0,10])
                rotate([0,-90,0])
                    NutM2(tolerance=0.2);
            translate([-5.5,0,5])
                rotate([0,-90,0])
                    NutM2(tolerance=0.2);
        }
            
       translate([-8.8,0,5])
            rotate([0,-90,0])
            {
                BoltM2(l=5);
            }
	}

    // arm axis
    
	translate([length-2.5,0,5+5-0.3])
	{
		cylinder(h=4,d=4);
        translate([0,0,4.0-0.01])
            metric_thread (diameter=3-0.2, pitch=0.5, length=2);
	}
}

module handle_face(diameter,length, width) 
{
    hull()
    {
        translate([length-2.5,0,0]) 
            cylinder(h=width,d=5);
        cylinder(h=width,d=diameter);
    }    
}
