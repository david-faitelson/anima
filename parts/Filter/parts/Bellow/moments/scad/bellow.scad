use <../../parts/Bed/moments/scad/bellow-bed.scad>
use <../../parts/Roof/moments/scad/bellow-roof.scad>
use <../../parts/Bag/moments/scad/bellow-bag.scad>

$fn = 128;

bellow(radius=50,height=25);

module bellow(radius = 50, height = 25)
{
    n = 8;
    
    bellow_angle = asin(height/(n*(radius/2)));

    translate([0,0,-height-2.5])
        rotate([0,0,30])
            bellow_bed();

    translate([0,0,3.5])
        bellow_roof();

    bellow_bag(radius=radius,opening_angle =bellow_angle,n=n);
}

