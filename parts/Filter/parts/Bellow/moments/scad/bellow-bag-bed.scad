use <../../parts/Bed/moments/scad/bellow-bed.scad>
use <../../parts/Roof/moments/scad/bellow-roof.scad>
use <../../parts/Bag/moments/scad/bellow-bag.scad>

bellow(radius=50,height=25);

module bellow(radius = 50, height = 25)
{
    bellow_angle = asin(height/(7*(radius/2)));

    translate([0,0,-height-4])
        rotate([0,0,30])
            bellow_bed();

    translate([0,0,2])
    rotate([0,0,30])
        bellow_roof();

    bellow_bag(radius=radius,opening_angle =bellow_angle,n=7);
}

