use <shapes.scad>

$fn = 128;

bellow_bed();

module bellow_bed()
{
    color("orange")
        ring(height=2,outer_diameter=100,outer_diameter_bottom=100, inner_diameter=84, nfaces =6); 
    
    color("orange")
    for(i=[0:5])
    {
        rotate([0,0,30+i*360/6])
            translate([44,0,2]) 
            {
                scale([1,1,-1])
                {
                    cylinder(h=6.5,r=2-0.1);
                    translate([0,0,4.5]) cylinder(h=2,r=3-0.1);
                }
            }
    }
}

