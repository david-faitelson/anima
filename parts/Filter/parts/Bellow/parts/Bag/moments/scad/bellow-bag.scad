use <Write/Write.scad>

module basic_triangle(base = 86.6, height=25) 
{
	linear_extrude(height=1)
		polygon(points=[[-base/2,0],[base/2,0],[0,height]]);
}

module hexagonal_triangle(height = 25) 
{
    basic_triangle(base = height*2*tan(60), height=height);
}

module bellow_bag(radius=50,opening_angle=15, n=6)
{
	for(i=[0:1:5])
	{
		rotate([0,0,i*60])
			translate([0,radius/2,0])
				hexagonal_triangle(height = radius/2);
	}
	
	for(j=[0:n-1])
	{
		rotate([0,0,60*j])
		{
			translate([0,0,-sin(opening_angle)*(radius/2)*j])
			{
				for(i=[0:1:2])
				{
					rotate([0,0,i*120])
						translate([0,radius/2,0]) 
						{
							rotate([-opening_angle,0,0])
								hexagonal_triangle(height = radius/2);
						}
				}
				
				translate([0,0,-sin(opening_angle)*(radius/2)])
				rotate([0,0,60])
				for(i=[0:1:2])
				{
					rotate([0,0,i*120])
						translate([0,radius/2,0])
							rotate([opening_angle,0,0])
								hexagonal_triangle(height = radius/2);
				}
			}
		}
	}
	
	translate([0,0,-sin(opening_angle)*(radius/2)*n])
	for(i=[0:1:5])
	{
		rotate([0,0,i*60])
			translate([0,radius/2,0])
				hexagonal_triangle(height = radius/2);
	}
}

function map(x,b1,e1,b2,e2) =
	b2 + (x-b1)*(e2-b2)/(e1-b1);

bellow_radius = 50;

bellow_height = 50;

bellow_angle = asin(bellow_height/(7*(bellow_radius/2)));

!bellow_bag(radius=bellow_radius,opening_angle=bellow_angle,n=8);

color("red")
translate([bellow_radius, 0, -bellow_height/2])    rotate([0,90,0])
        write(str(bellow_height),t=1,h=5,center=true, font = "orbitron.dxf"); 

color("red")
translate([bellow_radius+3,0,0])
    rotate([0,0,90])
        write(str(bellow_radius),t=1,h=5,center=true, font = "orbitron.dxf"); 

color("red")
translate([0,0,0])
    rotate([0,0,60])
        translate([0,-20,0])write(str(bellow_radius * tan(60)),t=1,h=5,center=true, font = "orbitron.dxf"); 
