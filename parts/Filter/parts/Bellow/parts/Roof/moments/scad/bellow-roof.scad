use <shapes.scad>
use <../../../Bed/moments/scad/bellow-bed.scad>

$fn = 128;

bellow_roof();

module bellow_roof()
{
    rotate([0,0,30])
        scale([1,1,-1])
            bellow_bed();

}

