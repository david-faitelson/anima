// Define pin connections & motor's steps per revolution

const int stepPin = 5; // 9 for unit 2, 5 for unit 1, 7 for unit 3
const int stepsPerRevolution = 200;

long start;
int step_delay;

void setup()
{
  // Declare pins as Outputs
  pinMode(stepPin, OUTPUT);

  start = millis();
  step_delay = 50;
  delay(4000);
}

void loop()
{
      digitalWrite(stepPin, HIGH);
      delayMicroseconds(100);
      digitalWrite(stepPin, LOW);
      //delayMicroseconds(2400);
      delay(10);
  /*
  for(int i =0;i<200;i++)
  {
      digitalWrite(stepPin, HIGH);
      delay(1);
      digitalWrite(stepPin, LOW);
      delay(1);
  }
  delay(2000);
  */
/*      delay(step_delay);
      if (step_delay > 12)
        step_delay = 30 - (millis() - start)/1000;
        */
}
