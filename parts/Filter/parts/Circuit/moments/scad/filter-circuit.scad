
filter_circuit();

module filter_circuit()
{
    color("green",1)
    rotate([0,0,-90])
        translate([-115,78,-0.5]) import("../stl/filter-circuit.stl",convexity=3);
    
    translate([0,-10,13])color("silver")cube([8,8,5]);
    
    translate([0,0,1])cube([1,1,8+4]);
}




