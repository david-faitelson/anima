use <shapes.scad>
use <pin.scad>
use <slice.scad>
use <math.scad>
use <shell.scad>
use <../../parts/BellowBase/moments/scad/bellow-base.scad>

$fn=128;
                    
angle = 8; //map(x=sin($t*360),b1=-1,e1=1,b2=0,e2=8);

//translate([0,0,12.2]) rotate([0,0,30+angle]) bellow_support();

//slice([-1.0,0,0],size=130)
    //slice([0.0,0,0],size=130)
        case();

*#translate([-26,0,22.5+3])
    cube([30,60,40],center=true);

*#translate([-30,0,22.5+1])
    cube([2,50,30],center=true);

// modules

module case()
{

	color("green",0.9)
        translate([0,0,47])
            bellow_base();
	
    shell();
    
    pcb_support();
    
    motor_support();

}

module pcb_support()
{
	difference()
	{
		intersection()
		{
			translate([-39,0,1.0 + 31 - 7.5])
            {
                cube([2,130,49],center=true);
            }
			receptacle(end_height=63.5,step_size=1);
		}
        
        translate([7,-2.5,40])
            cube([5,5,13]);
       
        *translate([-38-0.5,0,49])
            rotate([0,90,0])
                scale([1.618,1,1])
                    cylinder(h=3,d=40,$fn=8);
        
        // pcb mount holes
        
        translate([-39-2, 0, 29.5]) 
            for(i=[-1:2:1])
            {
                for(j=[-1:2:1])
                {
                    translate([0,i*22,j*12])
                        rotate([0,90,0])
                            cylinder(h=4,d=3+0.5,$fn=8);
                }
            }
        
	}
}

module motor_support() 
{
    offset = 2;
    width = 2.3;
    
	difference()
	{
		intersection()
		{
			translate([offset+2.5,0,30])
            {
                union()
                {
                    translate([0,0,-3.5])
                        cube([width,130,45],center=true);
                    translate([-width/2,0,-36])
                        rotate([0,90,0])
                        {
                            difference()
                            {
                                cylinder(h=width,d=120);
                                translate([-10,-60,-0.5])
                                    cube([60,120,4]);
                            }
                        }
                }
            }
			receptacle(end_height=63.5,step_size=1);
		}
	
        // holes for fingers (to access the nuts & bolts)
        
        for(i=[-1:2:1])
        translate([offset,35*i,30])
            rotate([0,90,0])
                cylinder(h=5,d=25);
        
        // a vertical hole to insert the motor shaft
        
       hull()
        {
            translate([offset,-12,54])
                cube([5,24,1]);
            translate([offset,-8,36])
                cube([5,16,1]);
        }

		translate([offset,0,31]) 
        {
            // handle hole
            
			rotate([0,90,0]) 
            {
                cylinder(h=5, d = 20);
                
            }
            // damper bolt holes
            stepper_holes();
        }

	}
}


module stepper_holes()
{
    for(i=[-1:2:1])
        for(j=[-1:2:1])
        {
            translate([0,15.5*i,15.5*j])
                rotate([0,90,0]) cylinder(h=5,d=3.5+0.2);
        }
}

module stepper_driver_pins()
{
	translate([15.0,31,28.5]) 
		rotate([0,-90,0])
            {
                translate([-11.1, -6.35,0])
					pin(6,2.0-0.1);
                translate([-11.1, 11.45,0])
					pin(6,2.0-0.1);
                translate([11.1, 10.8,0])
					pin(6,2.0-0.1);
            }
}

module solenoid_driver_pins()
{
	translate([15.0,-34.5,28.5]) 
		rotate([0,-90,0])
			for(i = [-1:2:1])
				for(j = [-1:2:1])
				{
					if (i == -1 && j == -1)
                        ;
                    else
                        translate([i*21.59/2,j*11.43/2,0])
                            pin(6,2.0-0.1);
				}
}
