use <shapes.scad>

$fn = 128;

bellow_base();

module bellow_base(tolerance=0.4)
{
    rotate([0,0,30])
    {
        difference()
        {
            ring(height=2, outer_diameter=126, outer_diameter_bottom=124,inner_diameter=88, nfaces=6);

            for(i=[0:5])
            {
                rotate([0,0,30+i*360/6])
                    translate([44,0,-0.5]) 
                    {
                        cylinder(h=3,d=4+tolerance);    
                    }
                rotate([0,0,30+8+i*360/6])
                    translate([44,0,-0.5]) 
                    {
                        cylinder(h=3,d=6+tolerance);
                    }
            }
            
            translate([0,0,-0.5])
            for(i=[0:5])
            {
                rotate([0,0,30+i*360/6])
                {
                    rotate_extrude(angle=8)
                        translate([42-tolerance/2,0]) square([4+tolerance,3]);
                }
            }
        }
    }
}

