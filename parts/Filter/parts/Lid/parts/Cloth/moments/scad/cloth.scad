
cloth();

module cloth(diameter=85)
{
	color("blue",0.5)
		rotate([0,0,30])cylinder(h=6,d=diameter,$fn=6);
}
