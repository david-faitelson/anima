use <Metric/files/M2.scad>
use <threads.scad>

$fn=128;

piston_axis();


module piston_axis(width = 6)
{
    
    *translate([7.5,0,0])
        rotate([0,90,0])
            color("red") BoltM2(h_head=1,l=4);
    
    color("blue")
    difference()
    {
        union()
        {
            hull()
            {
                translate([0,0,0])
                    cylinder(h=width,d=6.0,center=true);
                translate([2.5,0,0])
                    cube([3.0, 15, width], center= true);
            }
            translate([-1.5,0,1]) 
                shaft(width+4.9);
        }    
        // hole for M2 nut
        
        hull()
        {
            translate([2.5,0,-10])
                rotate([0,90,0])
                    NutM2(tolerance=0.2);
            translate([2.5,0,0])
                rotate([0,90,0])
                    NutM2(tolerance=0.2);
        }
        
        // hole for M2 bolt
        translate([14,0,0])rotate([0,90,0])
            BoltM2();
    }
}


module shaft(length)
{
    color("white")
    translate([0.5,0,-4])
    {
        cylinder(h=length,d=3.9);
        translate([0,0,length-0.01])
            metric_thread (diameter=3-0.2, pitch=0.5, length=2);
    }
}

