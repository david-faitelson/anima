use <slice.scad>

$fn=128;

module solenoid(state)
{
    position = state == 1 ? -1.4 : -3.5-1.4;
    
    color("silver")
    translate([0,0,-10 + position])
    {
        cylinder(h=1.4,r=1.5);
        cylinder(h=1.4+3.5,r=0.8);
        translate([0,0,1.4+3.5])
            cylinder(h=29.1-1.4-3.5,r=2);
        translate([0,0,29.1-2.0])
            cylinder(h=0.58,r=3);
    }
    translate([0,0,-10+2])
        color("blue",0.7)
            cylinder(h=20-4,d=11-2);
    difference()
    {
        color("silver",0.7)
            cube([11,12,20.0],center=true);
        translate([-1.0,0,0])
            cube([12,10.0,18.0],center=true);
        translate([3,12/2+1,-10+5])
            rotate([90,0,0])
                cylinder(h=3,d=1.5);
        translate([-3,12/2+1,-10+5+10])
            rotate([90,0,0])
                cylinder(h=3,d=1.5);
    }
}

//slice([0,-0.98,0])
    solenoid(state=1);

