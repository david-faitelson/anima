$fn = 128;

ejector_clip();

module ejector_clip()
{
            difference()
            {
                cylinder(h=2,d=30,$fn=6);
                translate([0,0,-0.5])
                    cylinder(h=1.4,d=3+0.4);
            }
}
