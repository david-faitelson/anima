use <shapes.scad>
use <slice.scad>
use <fillet.scad>
use <Metric/files/M2.scad>
use <../../parts/PistonAxis/moments/scad/piston-axis.scad>
use <../../parts/Cloth/moments/scad/cloth.scad>
use <../../parts/Ejector/moments/scad/ejector.scad>
use <../../../Solenoid/moments/scad/solenoid.scad>

$fn = 128;

//slice([1.0,0,0])
    //slice([0,-1,0])

lid(diameter=114);

module cloth_ejector(state)
{
    ejection_height = 
        state == 1 ? 3 : 0;
    
    translate([0,0,-7.5])
        rotate([0,180,90])  
            solenoid(state = state == 1 ? 0 : 1);
    
    *translate([0,0,3.1+ejection_height])
        rotate([0,0,30])
            ejector_clip();
    *translate([0,0,3.3  +ejection_height*1.6  ])
        cloth(diameter=106);
}

module lid(diameter = 80)
{
    *cloth_ejector(state=0);
    
    rotate([0,0,30])
    {
        difference()
        {
            simple_ring(height=2, diameter=diameter,width= 24,nfaces=6);
            
            for(i=[0:5])
            {
                rotate([0,0,30+i*360/6])
                    translate([44,0,-0.5]) 
                    {
                        cylinder(h=3,d=4+0.4);    
                    }
                rotate([0,0,30+8+i*360/6])
                    translate([44,0,-0.5]) 
                    {
                        cylinder(h=3,d=6+0.4);
                    }
            }
            
            translate([0,0,-0.5])
                for(i=[0:5])
                {
                    rotate([0,0,30+i*360/6])
                    {
                        rotate_extrude(angle=8)
                            translate([42-0.2,0]) square([4+0.4,3]);
                    }
                }
        }
        
        simple_ring(height=5.01, 
            diameter=diameter,
            width=5, 
            nfaces=6);
        
        // the top is a bit narrower to better hold the cloth
        translate([0,0,5.0]) 
            simple_ring(height=1, 
                diameter=diameter,
                width=6, 
                nfaces=6);
    }

    difference()
    {
        union()
        {
            translate([0,0,2.5]) 
                spokes(diameter);
            translate([0,0,0])
                rotate([0,0,30]) 
                    cylinder(h=3,r=15,$fn=6);
        }
        translate([0,0,3.5])
            cube([15.0,15.0,8],center=true);
    }
    
    translate([0,0,-11.5])
        rotate([0,180,90])
            box_fillet(width=15,height=15.0,depth=11.5, fillet_radius=1.5)
                rotate([0,90,0])
                    solenoid_house(height=29);
}

module spokes(diameter)
{   
	for(i = [0:5])
	{
		rotate([0,0,60*i])
		{
			translate([0,0,-1])
                cube([4,diameter-3,3],center = true);
			*translate([0,diameter/2,-1])
				cylinder(h=3,r=4.5,center=true);

		}
	}
}

module solenoid_house(height=27)
{
    difference()
    {
        cube([height+0,12+3,11.0+4],center=true);
        translate([1.5,0,0])
        {
            cube([height,12+0.5,11.0+2],center=true);
            translate([-12,-6.5,0])
                cube([10,3,16],center=true);
        }
        
        // solenoid screw holes
        
        translate([-1,8,-3])
            rotate([90,0,0])
                cylinder(h=4,d=1.5+0.2);
        
        translate([9,8,3])
            rotate([90,0,0])
                cylinder(h=4,d=1.5+0.2);
        
        // cable hole
        
        translate([6.5,0,-8])
            rotate([0,0,0])
                cylinder(h=3,d=6);
        
        // grove for solenoid rod and piston bolt
        translate([-13.5,0,0])
        {
            rotate([0,90,0])
            {
                cylinder(h=1.1,d=4.5+0.2);
                translate([0,0,4])BoltM2();
            }
        }
    }
    
    translate([-13,12.2/2,-14/2 ])
        rotate([90,-90,90])
            linear_fillet(length=14,radius=2);

    translate([-height/2-4.0-0.01,-0.5,0])
        rotate([-90,0,0])
            piston_axis();
}
