use <threads.scad>
use <shapes.scad>
use <fillet.scad>
use <slice.scad>
use <math.scad>

$fn = 128;

*!
union()
{
    branch(height = 111, 
        diameter = 18.5, 
        width = 3, 
        thread_tolerance=1.0);
}

angle = map(sin(360*$t),-1,1,0,360);

slice([0,1,0])
{
    union()
    {
        rotate([0,0,angle])
            branch(height = 180, diameter = 40, width = 3);
        translate([0,0,-186+0])
        branch(height = 180, diameter = 40, width = 3);
    }
}

module branch(height, diameter, width, thread_tolerance=1.0)
{
	thread_length = 10;
    
    bottom_thread_diameter = 0.9 * diameter - thread_tolerance;
    
    inner_diameter = diameter - 0.1*diameter -width*2 - 2;

    bottom_thread_width = (bottom_thread_diameter - inner_diameter)/2;

color("white")
    translate([0,0,-0.01])
        //scale([1,1,-1])
            bottom_thread(thread_length, bottom_thread_diameter, bottom_thread_width);
    
    difference() 
    {
        tube(height, diameter, width);

        translate([0,0,height - thread_length+0.1])
            metric_thread(length=thread_length, diameter = diameter* 0.9,pitch = 2);
    }

    tube(4, diameter, 0.1*diameter/2+width+1);
    
    
    translate([0,0,4-0.01])
        scale([1,1,2])
        inner_circular_fillet(radius=diameter/2-width+0.01,fillet_radius=0.15*diameter/2);
}

module bottom_thread(depth, diameter, width)
{
	translate([0,0,-depth])
		hollow_thread(height=depth, diameter = diameter, width = width, pitch = 2);
}
