use <threads.scad>
use <shapes.scad>
use <fillet.scad>
use <slice.scad>
use <math.scad>
use <branch.scad>

/*
$fn = 128;

//slice([0,1,0],size=300)
//slice([0,-1.01,0],size=300)
union()
{
    for (i=[3:4])
        translate([-60+25*i,0,0])
        {
            branch(
                height = 20, 
                diameter = 18.5, 
                width = 3,
                thread_tolerance=0.5*i);
            
            *translate([0,0,-23])
                branch(
                    height = 20, 
                    diameter = 18.5, 
                    width = 3);
        }
}
*/


$fn = 32;

angle = map(sin(360*$t),-1,1,-180,180);

*translate([-3,0,-3.4]) cube([10,10,2]);
*translate([23,0,-2.4]) cube([10,10,2]);

slice([0,1,0],size=300)
slice([0,-1.005,0],size=300)
union()
{
    for (i=[2:2])
        translate([-60+25*i,0,0])
        {
            rotate([0,0,-angle])
            translate([0,0,-1*sin(angle/2)])
            branch(
                height = 20 ,  
                diameter = 18.5, 
                width = 3,
                thread_tolerance=0.5*i);
            
            translate([0,0,-24.0])
                branch(
                    height = 20 , 
                    diameter = 18.5, 
                    width = 3);
        }
}



