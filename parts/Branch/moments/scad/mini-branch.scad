use <threads.scad>
use <shapes.scad>
use <fillet.scad>
use <slice.scad>
use <math.scad>
use <branch.scad>

$fn = 128;

//slice([0,1,0])
{
branch(height = 111, diameter = 18.5, width = 3);
}