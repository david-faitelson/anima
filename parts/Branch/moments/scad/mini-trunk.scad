use <threads.scad>
use <shapes.scad>
use <fillet.scad>
use <slice.scad>
use <math.scad>
use <branch.scad>

$fn = 128;

//slice([0,1,0])
{
    branch(height = 180, diameter = 40, width = 3);
}
