use <threads.scad>
use <shapes.scad>
use <slice.scad>

$fn = 128;

//slice([1,0,0])
fork_dome(diameter = 40, branch_angle = 45, num_branches = 3, branch_diameter = (40 / sqrt(3)) * 0.8);

module fork_dome(diameter, branch_angle, num_branches, branch_diameter) 
{
    
    difference()
    {
        union()
        {
            hollow_dome(diameter, diameter*0.15);
            simple_ring(height=2,diameter=diameter*0.9, width=0.37*diameter);
        }
        
        for(i=[0:2])
        {
            rotate([0,0,30+360/3*i])
                translate([diameter/2-5.5,0,-0.5])
                    cylinder(h=3.5,d=3);
        }
        for(i = [0:num_branches-1]) 
        {
            rotate([branch_angle,0,(i-0.5)*360/num_branches])
                translate([0,0,diameter*0.25]) 
                {
                    cylinder(h=diameter,d=branch_diameter+0.25);
                }
        }
    }
}
