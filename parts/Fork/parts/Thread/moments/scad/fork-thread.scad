use <threads.scad>
use <shapes.scad>
use <slice.scad>

$fn = 128;



//slice([1,0,0])
fork_thread(diameter = 40);

module fork_thread(diameter, thread_length=10) 
{
	color("silver",0.99)
	{
        for(i=[0:2])
        {
            rotate([0,0,360/3*i])
                translate([diameter/2-5.5,0,-0.5])
                    cylinder(h=3,d=2.5);
        }
        
		translate([0,0,-thread_length])
			hollow_thread(diameter=diameter*0.9 -1.0, height=thread_length,width=6,pitch=2);
    }
}
