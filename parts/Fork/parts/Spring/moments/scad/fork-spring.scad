use <threads.scad>
use <shapes.scad>
use <slice.scad>

$fn = 128;

slice([1,0,0])
petiol(height=8, diameter = (40 / sqrt(3)) * 0.8);

module petiol(height, diameter)
{
	ring_width = 20;

	color("black")
	difference()
	{
		cylinder(h=height,r=diameter/2);
		translate([0,0,-0.1*height]) 
		{
			metric_thread(diameter=diameter*0.9, pitch=2, length=1.2*height);
		}

	}
}


