use <threads.scad>
use <shapes.scad>
use <slice.scad>
use <../../parts/Dome/moments/scad/fork-dome.scad>
use <../../parts/Thread/moments/scad/fork-thread.scad>
use <../../parts/Spring/moments/scad/fork-spring.scad>

$fn = 32;

//slice([-1,0,0])
fork(diameter = 40, branch_angle = 45, num_branches = 3, branch_diameter = (40 / sqrt(3)) * 0.8);

module fork(diameter, branch_angle, num_branches, branch_diameter) 
{
	thread_length = 10;

	petiol_height=diameter/5;

    fork_dome(diameter=diameter,branch_angle=branch_angle,num_branches=num_branches,branch_diameter=branch_diameter);
    
    fork_thread(thread_length=thread_length,diameter=diameter);
    
	for(i = [0:num_branches-1]) 
	{
		rotate([branch_angle,0,(i-0.5)*360/num_branches])
			translate([0,0,diameter/2 -petiol_height]) 
				petiol(height=petiol_height,diameter=branch_diameter+0.1);
	}

}
