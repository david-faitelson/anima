use <slice.scad>
use <assemble.scad>

use <../../parts/Dome/moments/scad/fork-dome.scad>
use <../../parts/Thread/moments/scad/fork-thread.scad>
use <../../parts/Spring/moments/scad/fork-spring.scad>

$fn = 32;

slice([1,0,0])
fork_assembly(step=false);

module fork_assembly(step=false) 
{
	thread_length = 10;

    diameter=40;
    branch_diameter=0.8*diameter/sqrt(3);
    num_branches = 3;
    branch_angle = 45;

	petiol_height=diameter/5;

    rotate([0,0,30])fork_thread(thread_length=thread_length,diameter=diameter);
    
    path = [ 
        [ [0,0,30], [0,0,0] ], // fork dome
        [ [0,0,60], [0,0,0] ] // petiols
    
    ];

    assemble(path=path,step=step)
    {   
        color("white")
            fork_dome(diameter=diameter,branch_angle=branch_angle,num_branches=num_branches,branch_diameter=branch_diameter);
    
        for(i = [0:num_branches-1]) 
        {
            rotate([branch_angle,0,(i-0.5)*360/num_branches])
                translate([0,0,diameter/2 -petiol_height]) 
                    petiol(height=petiol_height,diameter=branch_diameter+0.1);
        }
    }
}
