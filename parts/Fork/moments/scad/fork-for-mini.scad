use <slice.scad>
use <fork.scad>

$fn = 128;

diameter=40;
branch_diameter=0.8*diameter/sqrt(3);
num_branches = 3;
branch_angle = 45;
 
fork(diameter=diameter,branch_angle=branch_angle,branch_diameter=branch_diameter,num_branches=num_branches);
